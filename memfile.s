INICIA
		AND		R5, R5, #0      ;Zera registradores
		AND		R7, R7, #0
		AND		R0, R0, #0
		AND		R4, R4, #0
		
		ADD     R5, R5, #1      ;R5 recebe o numero base da PG
		ADD		R7, R7, R5	    ;R7 = registrador da dupla
		ADD		R0, R0, #200
		ADD		R0, R0, #200
		ADD		R0, R0, #200
		ADD		R0, R0, #100    ;seta a posicao base da memoria em 700

		ADD		R4, R4, #1  	;inicia contador
		STR		R7, [R0]     	;salva primeiro valor da PG na memoria
		
LOOP
		TST		R7, #2147483648 ;verifica se o valor da PG é maior que 2^31
		BNE		FIM             ;se for, finaliza o codigo
		LSL		R7, R7, #1  	;senão, multiplica por 2
		ADD		R4, R4, #1	    ;incrementa o contador
		ADD		R0, R0, #4  	;incrementa endereco de memoria
		STR		R7, [R0]        ;salva valor na memoria
		CMP		R4, #10	    	;verifica se fez 10 iteracoes
		BEQ		FIM             ;caso tenha feito, finaliza o codigo
		B		LOOP            ;caso contrario, reinicia o loop
FIM
